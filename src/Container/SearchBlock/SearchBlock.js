import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import MovieLink from "../../Components/MovieLink/MovieLink";
import Spinner from "../../Components/UI/Spinner/Spinner";
import { CHANGE, fetchMovies } from "../../store/actions";
import "./SearchBlock.css";

const SearchBlock = (props) => {
    const dispatch = useDispatch();
    const state = useSelector((state) => state);

    useEffect(() => {
        dispatch(fetchMovies(state.inputValue));
    }, [dispatch, state.inputValue]);

    return (
        <>
            <header className="Header">
                <NavLink to="/">TV SHOWS</NavLink>
            </header>
            <main className="SearchBlock">
                <label>
                    Search for TV Show:
                    <input
                        type="text"
                        value={state.inputValue}
                        onChange={(e) => dispatch({ type: CHANGE, value: e.target.value })}
                    />
                </label>
                {state.loading ? <Spinner /> : null}
                <div className="Content">
                    <div>
                        {state.movies.map((movie) => {
                            return (
                                <MovieLink
                                    key={movie.show.id}
                                    id={movie.show.id}
                                    name={movie.show.name}
                                />
                            );
                        })}
                    </div>
                    {props.children}
                </div>
            </main>
        </>
    );
};

export default SearchBlock;
