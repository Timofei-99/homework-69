import React from "react";
import { Route, Switch } from "react-router-dom";
import SearchBlock from "./Container/SearchBlock/SearchBlock";
import FullInfo from "./Container/FullInfo/FullInfo";

const App = () => (
    <SearchBlock>
      <Switch>
        <Route path="/shows/:id" component={FullInfo} />
      </Switch>
    </SearchBlock>
);

export default App;
