import axiosMovies from "../axios-movies";

export const CHANGE = "CHANGE";

export const FETCH_MOVIES_REQUEST = "FETCH_MOVIES_REQUEST";
export const FETCH_MOVIES_SUCCESS = "FETCH_MOVIES_SUCCESS";
export const FETCH_MOVIES_FAILURE = "FETCH_MOVIES_FAILURE";

export const FETCH_SOLO_MOVIES_SUCCESS = "FETCH_SOLO_MOVIES_SUCCESS";

export const fetchMoviesRequest = () => ({ type: FETCH_MOVIES_REQUEST });

export const fetchMoviesSuccess = (movies) => ({
    type: FETCH_MOVIES_SUCCESS,
    movies,
});

export const fetchSoloMoviesSuccess = (movie) => ({
    type: FETCH_SOLO_MOVIES_SUCCESS,
    movie,
});

export const fetchMoviesFailure = () => ({ type: FETCH_MOVIES_FAILURE });

export const fetchMovies = (value) => {
    return async (dispatch) => {
        dispatch(fetchMoviesRequest());
        try {
            const response = await axiosMovies.get("/search/shows?q=" + value);
            dispatch(fetchMoviesSuccess(response.data));
        } catch (e) {
            dispatch(fetchMoviesFailure());
        }
    };
};

export const fetchMoviesSolo = (value) => {
    return async (dispatch) => {
        dispatch(fetchMoviesRequest());
        try {
            const response = await axiosMovies.get("/shows/" + value);
            dispatch(fetchSoloMoviesSuccess(response.data));
        } catch (e) {
            dispatch(fetchMoviesFailure());
        }
    };
};
