import React, { useEffect } from "react";

import { useDispatch, useSelector } from "react-redux";
import { fetchMoviesSolo } from "../../store/actions";
import "./FullInfo.css";

const FullInfo = (props) => {
    const dispatch = useDispatch();
    const movie = useSelector((state) => state.movie);

    useEffect(() => {
        dispatch(fetchMoviesSolo(props.match.params.id));
    }, [props.match.params.id]);

    return (
        <div className="FullInfo">
            <div className="ImgBlock">
                <img src={movie.image ? movie.image.medium : null} alt="NON PHOTO" />
            </div>
            <div className="TextBlock">
                <h2>{movie.name}</h2>
                <p>{movie.summary}</p>
            </div>
        </div>
    );
};

export default FullInfo;
